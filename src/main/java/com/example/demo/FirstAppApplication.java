
package com.example.demo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class FirstAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstAppApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s", name);
	}
	//Adding comments for the new Dev branch
	//Dev is a new branch from master..
	//Adding more comments to create another commit
	/*
		*Multi part comment
		*Making the second commit
	 */
//fthgkll
	//third commit to dev
	//Making changes in the Master branch
	public int doNothing(){
		//this function does nothing.
		return 0;
		/*
		This function was added just in case for the next exercise and now for master branch commit.
		 */
		//this comment is for the new branch dev-issue1 branch which was branched from master
		/*
		Adding one more commit to master branch
		 */
	}
}

//Adding in temp
